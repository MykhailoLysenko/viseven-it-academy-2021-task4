'use strict'
/*Задачи по теме JSON*/

/*==================================================================================*/

/*1)Взять один объект пользователя из задачи про Массивы. Преобразовать его в строку.*/

/*---------------------------------------------------------------------------------*/
const user = {
    name: "Alex",
    yearOfBirth : 1980,
    role: "admin",
    permission: "read, write, update, delete",
}

const userToString = JSON.stringify(user);
console.log(typeof userToString);
console.log(userToString);

/*---------------------------------------------------------------------------------*/

/*2)Полученные данные преобразовать обратно в объект.*/

/*---------------------------------------------------------------------------------*/

const userBackToOdject = JSON.parse(userToString);
console.log(typeof userBackToOdject);
console.log(userBackToOdject);

/*---------------------------------------------------------------------------------*/

/*3) Добавить метод toJSON в объект пользователя и сделать вывод только name и yearOfBirth во время преобразования методом JSON.stringify и выполнить преобразованиии проверить.*/

/*---------------------------------------------------------------------------------*/

user.toJSON = function() {
    return `name: ${this.name}, yearOfBirth: ${this.yearOfBirth}`;
}

console.log(JSON.stringify(user));

/*---------------------------------------------------------------------------------*/

/**4) Создать форму с полями Username, Email и Message. При нажатии на кнопку Submit собрать данные с полей формы и вывести в консоль данные в формате JSON.*/

/*---------------------------------------------------------------------------------*/

const userForm = document.querySelector(".user-form");
const inputFields = document.querySelectorAll(".user-name, .email, .user-message");


userForm.addEventListener("submit", (e) => {
    e.preventDefault();

    const objectForm = Array.from(inputFields).reduce((obj, field) => {
        obj[field.name] =  field.value;

        return obj;
    }, {});

    console.log(JSON.stringify(objectForm));
});

/*---------------------------------------------------------------------------------*/

