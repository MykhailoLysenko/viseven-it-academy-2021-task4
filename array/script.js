'use strict'
/*Задачи по теме Массивы*/

/*==================================================================================*/

/*1) Создать массив из 11 пользователей.
В каждом объекте пользователя должны быть свойства:
name,
yearOfBirth,
role (или "admin" или "moderator" или "client" или "guest"),
permission (
для admin - "write, read, update, delete",
для moderator - "write, read, update",
для client - "write, read",
для guest - "read"
)
При этом admin должен быть один, остальные пользователи могут иметь любую role.
P.S. Для создания массива пользователей можно взять за основу функцию генерации
пользователей из темы про объекты.*/

/*---------------------------------------------------------------------------------*/

let users = [];


const generateUser = (name, yearOfBirth, role) => {
    const user = {};
    user["name"] = name;
    user["yearOfBirth"] = yearOfBirth;
    user["role"] = role;

    switch (user["role"]) {
        case 'guest':
            user["permission"] = "read";
            break;
        case 'client':
            user["permission"] = "write, read";
            break;
        case 'moderator':
            user["permission"] = "write, read, update";
            break;
        default:
            user["permission"] = "write, read, update, delete";
    }

    return user;
}


const pushUsers = (arr, user) => {
    let flag = true;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].role === "admin")
            flag = false;
    }

    flag === false && user["role"] === "admin" ? console.log("WARNING : ADMIN IS ALREDY EXISTS, CHANGE OTHER ROLE") : arr.push(user);

    return arr;
}


pushUsers(users, generateUser("Alex", 1980, "admin"));
pushUsers(users, generateUser("Oleg", 1990, "moderator"));
pushUsers(users, generateUser("Nik", 1979, "moderator"));
pushUsers(users, generateUser("Ivan", 1996, "client"));
pushUsers(users, generateUser("Oksana", 2000, "guest"));
pushUsers(users, generateUser("Bohdan", 1982, "guest"));
pushUsers(users, generateUser("Marina", 1997, "client"));
pushUsers(users, generateUser("Olena", 1988, "guest"));
pushUsers(users, generateUser("Borys", 1981, "client"));
pushUsers(users, generateUser("Ruslan", 2006, "moderator"));
pushUsers(users, generateUser("Natali", 2002, "guest"));






/*---------------------------------------------------------------------------------*/

/*2) Свойство yearOfBirth хранит год рождения пользователя. К примеру 1992, 1999, 2003,
2000, 1986. Получить массив всех 11-ти пользователей, где будет посчитано сколько лет
каждому пользователю по текущему году. (То есть на выходе, учитывая даты описанные
выше, должен быть массив [29, 22, 18, 21, 35])*/

/*---------------------------------------------------------------------------------*/

const userAge = (arr, currentYear) => {
    let result = [];

    for (let key of arr) {
        result.push(currentYear - key.yearOfBirth);
    }
    return result;
}

const ageOfUsers = userAge(users, new Date().getFullYear());
console.log(ageOfUsers);


/*---------------------------------------------------------------------------------*/

/*3) Отсортировать пользователей по возрастанию, от младшего к старшему. (Задача 3 не
относиться ко 2-ой задаче, нужно вернуть массив объектов пользователей, а не просто
цифры)*/

/*---------------------------------------------------------------------------------*/

const usersSortedByAge = users.sort((a, b) => a.yearOfBirth - b.yearOfBirth);

console.log(usersSortedByAge);

/*---------------------------------------------------------------------------------*/

/*4) Отфильтровать пользователей младше 21 года (то есть получить всех пользователей
кому 21 и более лет)*/

/*---------------------------------------------------------------------------------*/

const usersOlderThen = users.filter(user => new Date().getFullYear() - user.yearOfBirth >= 21);

console.log(usersOlderThen);

/*---------------------------------------------------------------------------------*/

/*5) Отфильтровать и получить отдельные массивы для каждой категории пользователей (То
есть получить отдельно массив пользователей с role - moderator, отдельный массив с
client, отдельный массив для guest и один admin. На выходе должно быть 4 переменные
содержащие массивы для каждой role)*/

/*---------------------------------------------------------------------------------*/
const admins = users.filter(user => user.role === "admin");
const moderators = users.filter(user => user.role === "moderator");
const clients = users.filter(user => user.role === "client");
const guests = users.filter(user => user.role === "guest");

console.log(admins);
console.log(moderators);
console.log(clients);
console.log(guests);

/*---------------------------------------------------------------------------------*/

/**6) Получить всех пользователей кто может делать и write и update. (То есть
отфильтровать пользователей по полю permission при этом взять только тех, кто и write и
update.
При этом, если к примеру добавить дополнительную permission для определенного client,
то есть вместо "read, write" пользователь будет иметь "read, write, update", - то код
должен работать корректно и выдавать результат включая и этого клиента)*/

/*---------------------------------------------------------------------------------*/

const permissionedUsers = users.filter(user => user.permission.match(/write/) && user.permission.match(/update/));
console.log(permissionedUsers);


//TEST//  если к примеру добавить дополнительную permission для определенного client, то есть вместо "read, write" пользователь будет иметь "read, write, update"

const rewriteFistClientPermission = (usersArr) => {
    for(let key in usersArr) {
        if(usersArr[key].role === "client") {
        usersArr[key].permission = "read, write, update";
        break;
    }
}
    return usersArr;
}

const permissionedUsersPlusFrstClient = rewriteFistClientPermission(users).filter(user => user.permission.match(/write/) && user.permission.match(/update/));
console.log(permissionedUsersPlusFrstClient);

/*---------------------------------------------------------------------------------*/

/**7) Создать свою функцию reduce, которая в точности ведет себя как и метод массива
reduce. (Эта функция должна принимать 3 аргумента. Сам Массив, Функция обратного
вызова, Данные для инициализации) То есть эта функция должна быть полностью
идентична методу массива reduce и возвращать результат.
const reduce = (array, callback, initialValue) => {
// код функции
};*/

/*---------------------------------------------------------------------------------*/

const reduce = (array, callback, initialValue) => {
    initialValue =  typeof initialValue !== "number" ? 0 : initialValue;
    let accumulator = array[initialValue];
    
    for (let i = initialValue; i < array.length - 1; i++){
        let current = array[i + 1];
        accumulator = callback(accumulator, current);        
    }
    
    return accumulator;
};

const factorialOfArrNumb = reduce([1,2,3,4,5], (a, b) => a * b, );
console.log(factorialOfArrNumb);

/*---------------------------------------------------------------------------------*/

